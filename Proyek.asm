Cetak_Klm MACRO Klm 
          MOV   AH,09
          LEA   DX,Klm
          INT   21h
          ENDM  

CDesimal MACRO  Angka
         LOCAL  Ulang, Cetak
         MOV    AX,Angka 
         MOV    BX,10 
         XOR    CX,CX 

Ulang :
         XOR    DX,DX 
         DIV    BX 
         PUSH   DX 
         INC    CX 
         CMP    AX,0 
         JNE    Ulang 

Cetak :
         POP    DX 
         ADD    DL,'0' 
         MOV    AH,02 
         INT    21h 
         LOOP   Cetak 
         ENDM       


         CODE_SEG SEGMENT 
         ASSUME CS : CODE_SEG
         ORG 100H

TData : JMP     Awal
        Batas   DW 1000
        Prima   DW 0
        I       DW 2
        J       DW 2
    Spasi   DB ' $'
    Header  DB 9,9,9,'Bilangan Prima 1 sampai 1000 : ',13,10
            DB 9,9,9,'------------------------',13,10,10,'$'

Awal :
        Cetak_Klm Header
Proses :
        MOV     AX,Batas 
        CMP     AX,I 
        JE      Exit 
ForI :
        MOV     J,2 
        MOV     Prima,0 
ForPrima:
        MOV     AX,Prima    
        CMP     AX,0 
        JNE     TambahI 
        
        MOV     AX,I 
        CMP     AX,J 
        JNE     Tidak 
        CDesimal I 
        Cetak_Klm Spasi 
        MOV     Prima,1 
        JMP     TambahJ 
Tidak :
        MOV     DX,0 
        MOV     AX,I 
        MOV     BX,J 
        DIV     BX 
        CMP     DX,0 
        JNE     TambahJ 
        
        MOV     Prima,1 
TambahJ :
        INC J 
        JMP ForPrima 
TambahI :
        INC I 
        JMP Proses 
Exit :
        INT 20h
CODE_SEG ENDS
END     TData